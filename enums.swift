enum Fruit: String {
    case apple = "Apple"
    case orange = "Orange"
    case banana = "Banana"
}

enum Vegetable: Int {
    case carrot = 0
    case potato = 1
    case onion = 2
}

enum Gender: Int {
    case male = 0
    case female = 1
}

enum AgeCategory: Int {
    case young = 0
    case middleAged = 1
    case elderly = 2
}

enum Experience: Int {
    case Junior = 0
    case Middle = 1
    case Senior = 2
}

enum RainbowColor {
    case red, orange, yellow, green, blue, indigo, violet
}

func printEnumValues<T: RawRepresentable>(enumValues: [T]) {
    for enumValue in enumValues {
        print(enumValue.rawValue)
    }
}

var objects = ["apple", "sun", "table", "chair", "man", "PC", "pan"]
func colors (color: RainbowColor) {
    switch color {
        case .red:
            print("red \(objects[0])")
        case .orange:
            print("orange \(objects[1])")
        case .yellow:
            print("yellow \(objects[2])")
        case .green:
            print("green \(objects[3])")
        case .blue:
            print("blue \(objects[4])")
        case .indigo:
            print("indigo \(objects[5])")
        case .violet:
            print("violet \(objects[6])")
    }
}
colors(color: .orange)

enum Score: String {
    case noinfo = "Нет оценки"
    case fail = "Неудовлетворительно"
    case satisfactory = "Удовлетворительно"
    case good = "Хорошо"
    case excellent = "Отлично"
}

func grading(score: Score) {
    switch score {
        case .noinfo:
            print(0)
        case .fail:
            print(2)
        case .satisfactory:
            print(3)
        case .good:
            print(4)
        case .excellent:
            print(5)
    }
}

grading(score: Score(rawValue: "Отлично") ?? .noinfo)

enum cars : String, CaseIterable {
    case mercedes = "Mercedes"
    case lada = "Lada"
    case audi = "Audi"
    case haval = "Haval"
}

func printCars() {
    for car in cars.allCases {
        print(car.rawValue)
    }
}
printCars()